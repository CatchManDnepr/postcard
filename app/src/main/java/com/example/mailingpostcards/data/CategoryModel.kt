package com.example.mailingpostcards.data

import android.graphics.drawable.Drawable
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryModel(
    val id: Int,
    val title: String,
    val imgs: List<Int>,
    val code: String,
    val listOfSub: List<SubcategoryModel>,
    val isSelebration: Boolean
): Parcelable
