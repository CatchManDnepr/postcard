package com.example.mailingpostcards.data

import android.graphics.drawable.Drawable
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SubcategoryModel(
    val title: String,
    val imgs: List<Int>
): Parcelable
