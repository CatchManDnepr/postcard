package com.example.mailingpostcards

import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics

class App : Application() {


    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate() {
        super.onCreate()
        KoinProvider.startKoin(this)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }


}