package com.example.mailingpostcards.utils.stub

import android.app.Application
import com.example.mailingpostcards.base.BaseViewModel

class StubViewModel(
    private val app: Application
) : BaseViewModel(app) {

}