package com.example.mailingpostcards.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcelable
import com.example.mailingpostcards.R
import com.example.mailingpostcards.data.SubcategoryModel
import kotlinx.parcelize.Parcelize

@Parcelize
enum class Category: Parcelable {

    FOUR_JULY {
        override fun getImgs() = listOf(
            R.drawable.usa_4_july_50001,
            R.drawable.usa_4_july_50002,
            R.drawable.usa_4_july_50003,
            R.drawable.usa_4_july_50004,
            R.drawable.usa_4_july_50005
        )

        override fun getId() = 1

        override fun getTitle() = "4 July"

        override fun getSubCategory() = emptyList<SubcategoryModel>()

        override fun getCategoryCode() = "USA-4-July"
    },
    ADMINISTRATIVE_PROFESSIONALS_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_administrative_professionals_day_440001,
            R.drawable.usa_administrative_professionals_day_440002,
            R.drawable.usa_administrative_professionals_day_440003
        )

        override fun getId() = 2

        override fun getTitle() = "Administrative professional day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()

        override fun getCategoryCode() = "USA-Administrative-Professionals-Day"
    },
    AG_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_ag_day_400001,
            R.drawable.usa_ag_day_400002,
            R.drawable.usa_ag_day_400003,
            R.drawable.usa_ag_day_400004
        )

        override fun getId() = 3

        override fun getTitle() = "Agriculture day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()

        override fun getCategoryCode() = "USA-Ag-Day"
    },
    BIRTHDAY_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_birthday_all_760001,
            R.drawable.usa_birthday_usa_all_760002,
            R.drawable.usa_birthday_usa_all_760003,
            R.drawable.usa_birthday_usa_all_760004,
            R.drawable.usa_birthday_usa_all_760005,
            R.drawable.usa_birthday_usa_all_760006
        )

        override fun getId() = 4

        override fun getTitle() = "Birthday"

        override fun getCategoryCode() = "USA-birthday"


        override fun getSubCategory() = listOf(
            SubcategoryModel(
                title = "Cute",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_cute_830001,
                    R.drawable.usa_birthday_usa_cute_830002
                )
            ),
            SubcategoryModel(
                title = "Dad",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_dad_770001,
                    R.drawable.usa_birthday_usa_dad_770002
                )
            ),
            SubcategoryModel(
                title = "Daughter",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_daughter_800001,
                    R.drawable.usa_birthday_usa_daughter_800002,
                    R.drawable.usa_birthday_usa_daughter_800003
                )
            ),
            SubcategoryModel(
                title = "Granddaughter",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_granddaughter_820001,
                    R.drawable.usa_birthday_usa_granddaughter_820002
                )
            ),
            SubcategoryModel(
                title = "Husband",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_husband_850001,
                    R.drawable.usa_birthday_usa_husband_850002
                )
            ),
            SubcategoryModel(
                title = "Mom",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_mom_780001,
                    R.drawable.usa_birthday_usa_mom_780002
                )
            ),
            SubcategoryModel(
                title = "Religious",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_religious_860001,
                    R.drawable.usa_birthday_usa_religious_860002
                )
            ),
            SubcategoryModel(
                title = "Son",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_son_790001,
                    R.drawable.usa_birthday_usa_son_790002
                )
            ),
            SubcategoryModel(
                title = "Wife",
                imgs = listOf(
                    R.drawable.usa_birthday_usa_wife_840001,
                    R.drawable.usa_birthday_usa_wife_840002
                )
            )
        )
    },
    BUSINESS_WOMAN_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_american_business_womens_day_600001,
            R.drawable.usa_american_business_womens_day_600002,
            R.drawable.usa_american_business_womens_day_600003,
            R.drawable.usa_american_business_womens_day_600004
        )

        override fun getId() = 5

        override fun getTitle() = "Business woman day"

        override fun getCategoryCode() = "USA-American-Business-Womens-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    ANNIVERSARY {
        override fun getImgs() = listOf(
            R.drawable.usa_anniversary_750001,
            R.drawable.usa_anniversary_750002,
            R.drawable.usa_anniversary_750003
        )

        override fun getId() = 6

        override fun getTitle() = "Anniversary"

        override fun getCategoryCode() = "USA-anniversary"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    APRIL_FOOLS_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_april_fools_day_210001,
            R.drawable.usa_april_fools_day_210002,
            R.drawable.usa_april_fools_day__210003
        )

        override fun getId() = 7

        override fun getTitle() = "April fools day"

        override fun getCategoryCode() = "USA-April-Fools-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    ARBOR_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_arbor_day_230001,
            R.drawable.usa_arbor_day_230002,
            R.drawable.usa_arbor_day_230003
        )

        override fun getId() = 7

        override fun getTitle() = "Arbor day"

        override fun getCategoryCode() = "USA-Arbor-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    ARMED_FORCES_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_armed_forces_day_490001,
            R.drawable.usa_armed_forces_day_490002
        )

        override fun getId() = 8

        override fun getTitle() = "Armed forces day"

        override fun getCategoryCode() = "USA-Armed-Forces-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    BARTENDER_APPRECIATION_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_bartender_appreciation_day_720001,
            R.drawable.usa_bartender_appreciation_day_720002,
            R.drawable.usa_bartender_appreciation_day_720003
        )

        override fun getId() = 9

        override fun getTitle() = "Bartender appreciation day"

        override fun getCategoryCode() = "USA-Bartender-Appreciation-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CAREER_NURSE_ASSISTANTS_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_career_nurse_assistants_day_510001,
            R.drawable.usa_career_nurse_assistants_day_510002
        )

        override fun getId() = 10

        override fun getTitle() = "Career nurse assistants day"

        override fun getCategoryCode() = "USA-Career-Nurse-Assistants-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CERTIFIED_NURSES_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_certified_nurses_day_390001,
            R.drawable.usa_certified_nurses_day_390002
        )

        override fun getId() = 11

        override fun getTitle() = "Certified nurses day"

        override fun getCategoryCode() = "USA-Certified-Nurses-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CHRISTMAS {
        override fun getImgs() = listOf(
            R.drawable.usa_christmas__10001,
            R.drawable.usa_christmas__10002,
            R.drawable.usa_christmas__10003,
            R.drawable.usa_christmas__10004,
            R.drawable.usa_christmas__10005,
            R.drawable.usa_christmas__10006
        )

        override fun getId() = 12

        override fun getTitle() = "Christmas"

        override fun getCategoryCode() = "USA-Christmas"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CINCO_DE_MAYO {
        override fun getImgs() = listOf(
            R.drawable.usa_cinco_de_mayo_250001,
            R.drawable.usa_cinco_de_mayo_250002,
            R.drawable.usa_cinco_de_mayo_250003
        )

        override fun getId() = 13

        override fun getTitle() = "Cinco-de-Mayo"

        override fun getCategoryCode() = "USA-Cinco-de-Mayo"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    COMMUNITY_MANAGER_DAY {
        override fun getImgs() = listOf(
            R.drawable.usa_community_manager_day_350001,
            R.drawable.usa_community_manager_day_350002
        )

        override fun getId() = 14

        override fun getTitle() = "Community manager day"

        override fun getCategoryCode() = "USA-Community-Manager-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CONDOLENCE {
        override fun getImgs() = listOf(
            R.drawable.usa_condolence__870001,
            R.drawable.usa_condolence__870002,
            R.drawable.usa_condolence__870003
        )

        override fun getId() = 15

        override fun getTitle() = "Condolence"

        override fun getCategoryCode() = "USA-Condolence"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CONGRATULATIONS {
        override fun getImgs() = listOf(
            R.drawable.congratulation__880001,
            R.drawable.congratulation__880002,
            R.drawable.congratulation__880003
        )

        override fun getId() = 16

        override fun getTitle() = "Congratulations"

        override fun getCategoryCode() = "USA-Congratulation"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CONSTITUTION_DAY {
        override fun getImgs() = listOf(
            R.drawable.constitution_day__310001,
            R.drawable.constitution_day__310002,
            R.drawable.constitution_day__310003
        )

        override fun getId() = 17

        override fun getTitle() = "Constitution day"

        override fun getCategoryCode() = "USA-Constitution-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    CUSTODIAN_DAY {
        override fun getImgs() = listOf(
            R.drawable.custodian_day__610001,
            R.drawable.custodian_day__610002
        )

        override fun getId() = 18

        override fun getTitle() = "Custodian day"

        override fun getCategoryCode() = "USA-Custodian-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    DARWIN_DAY {
        override fun getImgs() = listOf(
            R.drawable.darwin_day__370001,
            R.drawable.darwin_day__370002
        )

        override fun getId() = 19

        override fun getTitle() = "USA-Darwin-Day"

        override fun getCategoryCode() = "USA-Custodian-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    DAYPART_DAY {
        override fun getImgs() = listOf(
            R.drawable.dayparts_usa_good_morning_920001,
            R.drawable.dayparts_usa_good_morning_920002,
            R.drawable.dayparts_usa_good_morning_920003,
            R.drawable.dayparts_usa_good_afternoon_930001,
            R.drawable.dayparts_usa_good_afternoon_930002,
            R.drawable.dayparts_usa_good_afternoon_930003,
            R.drawable.dayparts_usa_good_evening_940001,
            R.drawable.dayparts_usa_good_evening_940002,
            R.drawable.dayparts_usa_good_evening_940003,
            R.drawable.dayparts_usa_good_night_950001,
            R.drawable.dayparts_usa_good_night_950002,
            R.drawable.dayparts_usa_good_night_950003
        )

        override fun getId() = 20

        override fun getTitle() = "Dayparts"

        override fun getCategoryCode() = "USA-Dayparts"

        override fun getSubCategory() = listOf(
            SubcategoryModel(
                title = "Good morning",
                imgs = listOf(
                    R.drawable.dayparts_usa_good_morning_920001,
                    R.drawable.dayparts_usa_good_morning_920002,
                    R.drawable.dayparts_usa_good_morning_920003
                )
            ),
            SubcategoryModel(
                title = "Good afternoon",
                imgs = listOf(
                    R.drawable.dayparts_usa_good_afternoon_930001,
                    R.drawable.dayparts_usa_good_afternoon_930002,
                    R.drawable.dayparts_usa_good_afternoon_930003
                )
            ),
            SubcategoryModel(
                title = "Good evening",
                imgs = listOf(
                    R.drawable.dayparts_usa_good_evening_940001,
                    R.drawable.dayparts_usa_good_evening_940002,
                    R.drawable.dayparts_usa_good_evening_940003
                )
            ),
            SubcategoryModel(
                title = "Good night",
                imgs = listOf(
                    R.drawable.dayparts_usa_good_night_950001,
                    R.drawable.dayparts_usa_good_night_950002,
                    R.drawable.dayparts_usa_good_night_950003
                )
            )
        )
    },
    DAYS_OF_WEEK_DAY {
        override fun getImgs() = listOf(
            R.drawable.days_of_week_usa_monday_1070001,
            R.drawable.days_of_week_usa_monday_1070002,
            R.drawable.days_of_week_usa_monday_1070003,
            R.drawable.days_of_week_usa_tuesday_1080001,
            R.drawable.days_of_week_usa_tuesday_1080002,
            R.drawable.days_of_week_usa_tuesday_1080003,
            R.drawable.days_of_week_usa_wednesday_1090001,
            R.drawable.days_of_week_usa_wednesday_1090002,
            R.drawable.days_of_week_usa_wednesday_1090003,
            R.drawable.days_of_week_usa_thursday_1100001,
            R.drawable.days_of_week_usa_thursday_1100002,
            R.drawable.days_of_week_usa_thursday_1100003,
            R.drawable.days_of_week_usa_thursday_1100004,
            R.drawable.days_of_week_usa_friday_1110001,
            R.drawable.days_of_week_usa_friday_1110002,
            R.drawable.days_of_week_usa_friday_1110003,
            R.drawable.days_of_week_usa_friday_1110004,
            R.drawable.days_of_week_usa_saturday_1120001,
            R.drawable.days_of_week_usa_saturday_1120002,
            R.drawable.days_of_week_usa_saturday_1120003,
            R.drawable.days_of_week_usa_sunday_1130001,
            R.drawable.days_of_week_usa_sunday_1130002,
            R.drawable.days_of_week_usa_sunday_1130003
        )

        override fun getId() = 21

        override fun getTitle() = "Days of week"

        override fun getCategoryCode() = "USA-Days-of-week"

        override fun getSubCategory() = listOf(
            SubcategoryModel(
                title = "Monday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_monday_1070001,
                    R.drawable.days_of_week_usa_monday_1070002,
                    R.drawable.days_of_week_usa_monday_1070003
                )
            ),
            SubcategoryModel(
                title = "Tuesday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_tuesday_1080001,
                    R.drawable.days_of_week_usa_tuesday_1080002,
                    R.drawable.days_of_week_usa_tuesday_1080003
                )
            ),
            SubcategoryModel(
                title = "Wednesday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_wednesday_1090001,
                    R.drawable.days_of_week_usa_wednesday_1090002,
                    R.drawable.days_of_week_usa_wednesday_1090003
                )
            ),
            SubcategoryModel(
                title = "Thursday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_thursday_1100001,
                    R.drawable.days_of_week_usa_thursday_1100002,
                    R.drawable.days_of_week_usa_thursday_1100003,
                    R.drawable.days_of_week_usa_thursday_1100004,
                    R.drawable.days_of_week_usa_thursday_1100005,
                    R.drawable.days_of_week_usa_thursday_1100006
                )
            ),
            SubcategoryModel(
                title = "Friday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_friday_1110001,
                    R.drawable.days_of_week_usa_friday_1110002,
                    R.drawable.days_of_week_usa_friday_1110003,
                    R.drawable.days_of_week_usa_friday_1110004
                )
            ),
            SubcategoryModel(
                title = "Saturday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_saturday_1120001,
                    R.drawable.days_of_week_usa_saturday_1120002,
                    R.drawable.days_of_week_usa_saturday_1120003
                )
            ),
            SubcategoryModel(
                title = "Sunday",
                imgs = listOf(
                    R.drawable.days_of_week_usa_sunday_1130001,
                    R.drawable.days_of_week_usa_sunday_1130002,
                    R.drawable.days_of_week_usa_sunday_1130003
                )
            )
        )
    },
    EARTH_DAY {
        override fun getImgs() = listOf(
            R.drawable.earth_day__220001,
            R.drawable.earth_day__220002,
            R.drawable.earth_day__220003
        )

        override fun getId() = 22

        override fun getTitle() = "Earth day"

        override fun getCategoryCode() = "USA-Earth-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    EASTER {
        override fun getImgs() = listOf(
            R.drawable.easter__40001,
            R.drawable.easter__40002,
            R.drawable.easter__40003,
            R.drawable.easter__40004,
            R.drawable.easter__40005,
            R.drawable.easter__40006
        )

        override fun getId() = 23

        override fun getTitle() = "Easter"

        override fun getCategoryCode() = "USA-Easter"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    EMPLOYEE_APPRECIATION_DAY {
        override fun getImgs() = listOf(
            R.drawable.employee_appreciation_day__380001,
            R.drawable.employee_appreciation_day__380002
        )

        override fun getId() = 24

        override fun getTitle() = "Employee appreciation day"

        override fun getCategoryCode() = "USA-Employee-Appreciation-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    EMS_WEEK {
        override fun getImgs() = listOf(
            R.drawable.ems_week__470001,
            R.drawable.ems_week__470002,
            R.drawable.ems_week__470003
        )

        override fun getId() = 25

        override fun getTitle() = "EMS week"

        override fun getCategoryCode() = "USA-EMS-Week"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    EPIPHANY {
        override fun getImgs() = listOf(
            R.drawable.epiphany__160001,
            R.drawable.epiphany__160002,
            R.drawable.epiphany__160003
        )

        override fun getId() = 26

        override fun getTitle() = "Epiphany"

        override fun getCategoryCode() = "USA-Epiphany"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    FAMILY {
        override fun getImgs() = listOf(
            R.drawable.family_baby_shower_1050001,
            R.drawable.family_baby_shower_1050002,
            R.drawable.family_baby_shower_1050003,
            R.drawable.family_engagement_1020001,
            R.drawable.family_engagement_1020002,
            R.drawable.family_engagement_1020003,
            R.drawable.family_newborn_1060001,
            R.drawable.family_newborn_1060002,
            R.drawable.family_newborn_1060003,
            R.drawable.family_wedding_1030001,
            R.drawable.family_wedding_1030002,
            R.drawable.family_wedding_1030003,
            R.drawable.family_wedding_anniversary_1040001,
            R.drawable.family_wedding_anniversary_1040002,
            R.drawable.family_wedding_anniversary_1040003
        )

        override fun getId() = 27

        override fun getTitle() = "Family"

        override fun getCategoryCode() = "USA-Family"

        override fun getSubCategory() = listOf(
            SubcategoryModel(
                title = "Baby shower",
                imgs = listOf(
                    R.drawable.family_baby_shower_1050001,
                    R.drawable.family_baby_shower_1050002,
                    R.drawable.family_baby_shower_1050003
                )
            ),
            SubcategoryModel(
                title = "Engagement",
                imgs = listOf(
                    R.drawable.family_engagement_1020001,
                    R.drawable.family_engagement_1020002,
                    R.drawable.family_engagement_1020003
                )
            ),
            SubcategoryModel(
                title = "Newborn",
                imgs = listOf(
                    R.drawable.family_newborn_1060001,
                    R.drawable.family_newborn_1060002,
                    R.drawable.family_newborn_1060003
                )
            ),
            SubcategoryModel(
                title = "Wedding",
                imgs = listOf(
                    R.drawable.family_wedding_1030001,
                    R.drawable.family_wedding_1030002,
                    R.drawable.family_wedding_1030003
                )
            ),
            SubcategoryModel(
                title = "Wedding anniversary",
                imgs = listOf(
                    R.drawable.family_wedding_anniversary_1040001,
                    R.drawable.family_wedding_anniversary_1040002,
                    R.drawable.family_wedding_anniversary_1040003
                )
            )
        )
    },
    FAREWELL {
        override fun getImgs() = listOf(
            R.drawable.farewell__890001,
            R.drawable.farewell__890002,
            R.drawable.farewell__890003
        )

        override fun getId() = 28

        override fun getTitle() = "Farewell"

        override fun getCategoryCode() = "USA-Farewell"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    FATHERS_DAY {
        override fun getImgs() = listOf(
            R.drawable.fathers_day__70001,
            R.drawable.fathers_day__70002,
            R.drawable.fathers_day__70003,
            R.drawable.fathers_day__70004,
            R.drawable.fathers_day__70005,
            R.drawable.fathers_day__70006
        )

        override fun getId() = 29

        override fun getTitle() = "Fathers day"

        override fun getCategoryCode() = "USA-Fathers-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    FIRST_RESPONDERS_DAY {
        override fun getImgs() = listOf(
            R.drawable.first_responders_day__650001,
            R.drawable.first_responders_day__650002,
            R.drawable.first_responders_day__650003
        )

        override fun getId() = 30

        override fun getTitle() = "First responders day"

        override fun getCategoryCode() = "USA-First-Responders-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    FLAG_DAY {
        override fun getImgs() = listOf(
            R.drawable.flag_day__260001,
            R.drawable.flag_day__260002,
            R.drawable.flag_day__260003
        )

        override fun getId() = 31

        override fun getTitle() = "Flag day"

        override fun getCategoryCode() = "USA-Flag-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    FRIENDSHIP {
        override fun getImgs() = listOf(
            R.drawable.friendship__900001,
            R.drawable.friendship__900002,
            R.drawable.friendship__900003
        )

        override fun getId() = 32

        override fun getTitle() = "Friendship"

        override fun getCategoryCode() = "USA-Friendship"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    GET_WELL_SOON {
        override fun getImgs() = listOf(
            R.drawable.get_well_soon__910001,
            R.drawable.get_well_soon__910002,
            R.drawable.get_well_soon__910003
        )

        override fun getId() = 33

        override fun getTitle() = "Get well soon"

        override fun getCategoryCode() = "USA-Get-Well-Soon"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    GOOD_FRIDAY {
        override fun getImgs() = listOf(
            R.drawable.good_friday__190001,
            R.drawable.good_friday__190002,
            R.drawable.good_friday__190003
        )

        override fun getId() = 34

        override fun getTitle() = "Good friday"

        override fun getCategoryCode() = "USA-Good-Friday"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    GRADUATION {
        override fun getImgs() = listOf(
            R.drawable.graduation__960001,
            R.drawable.graduation__960002,
            R.drawable.graduation__960003
        )

        override fun getId() = 35

        override fun getTitle() = "Graduation"

        override fun getCategoryCode() = "USA-Graduation"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    GROUNDHOG_DAY {
        override fun getImgs() = listOf(
            R.drawable.groundhog_day__200001,
            R.drawable.groundhog_day__200002,
            R.drawable.groundhog_day__200003
        )

        override fun getId() = 36

        override fun getTitle() = "Groundhog day"

        override fun getCategoryCode() = "USA-Groundhog-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    HAIRSTYLIST_APPRECIATION_DAY {
        override fun getImgs() = listOf(
            R.drawable.hairstylist_appreciation_day__430001,
            R.drawable.hairstylist_appreciation_day__430002
        )

        override fun getId() = 37

        override fun getTitle() = "Hairstylist appreciation day"

        override fun getCategoryCode() = "USA-Hairstylist-Appreciation-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    HALLOWEEN {
        override fun getImgs() = listOf(
            R.drawable.halloween__60001,
            R.drawable.halloween__60002,
            R.drawable.halloween__60003,
            R.drawable.halloween__60004,
            R.drawable.halloween__60005,
            R.drawable.halloween__60006
        )

        override fun getId() = 38

        override fun getTitle() = "Hallow"

        override fun getCategoryCode() = "USA-Halloween"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    HELEN_KELLER_DAY {
        override fun getImgs() = listOf(
            R.drawable.helen_keller_day__270001,
            R.drawable.helen_keller_day__270002,
            R.drawable.helen_keller_day__270003,
            R.drawable.helen_keller_day__270004
        )

        override fun getId() = 39

        override fun getTitle() = "Helen keller day"

        override fun getCategoryCode() = "USA-Helen-Keller-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    HVAC_DAY {
        override fun getImgs() = listOf(
            R.drawable.hvac_tech_day__520001,
            R.drawable.hvac_tech_day__520002,
            R.drawable.hvac_tech_day__520003
        )

        override fun getId() = 40

        override fun getTitle() = "HVAC day"

        override fun getCategoryCode() = "USA-HVAC-Tech-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    INTER_ACCOUNTING_DAY {
        override fun getImgs() = listOf(
            R.drawable.interaccounting_day__700001,
            R.drawable.interaccounting_day__700002,
            R.drawable.interaccounting_day__700003
        )

        override fun getId() = 41

        override fun getTitle() = "InterAccounting day"

        override fun getCategoryCode() = "USA-InterAccounting-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    INTER_COACHING_WEEK {
        override fun getImgs() = listOf(
            R.drawable.intercoaching_week__480001,
            R.drawable.intercoaching_week__480002
        )

        override fun getId() = 42

        override fun getTitle() = "InterCoaching day"

        override fun getCategoryCode() = "USA-InterCoaching-Week"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    IT_PROFESSIONALS_DAY {
        override fun getImgs() = listOf(
            R.drawable.it_professionals_day__590001,
            R.drawable.it_professionals_day__590002
        )

        override fun getId() = 43

        override fun getTitle() = "IT professionals day"

        override fun getCategoryCode() = "USA-IT-Professionals-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    KINDERGARTEN_DAY {
        override fun getImgs() = listOf(
            R.drawable.kindergarten_day__420001,
            R.drawable.kindergarten_day__420002,
            R.drawable.kindergarten_day__420003
        )

        override fun getId() = 44

        override fun getTitle() = "Kindergarten day"

        override fun getCategoryCode() = "USA-Kindergarten-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    LABOR_DAY {
        override fun getImgs() = listOf(
            R.drawable.labor_day__140001,
            R.drawable.labor_day__140002,
            R.drawable.labor_day__140003
        )

        override fun getId() = 45

        override fun getTitle() = "Labor day"

        override fun getCategoryCode() = "USA-Labor-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    LIBRARY_WORKERS_DAY {
        override fun getImgs() = listOf(
            R.drawable.library_workers_day__410001,
            R.drawable.library_workers_day__410002,
            R.drawable.library_workers_day__410003
        )

        override fun getId() = 46

        override fun getTitle() = "Library workers day"

        override fun getCategoryCode() = "USA-Library-Workers-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    LOVE {
        override fun getImgs() = listOf(
            R.drawable.love__980001,
            R.drawable.love__980002,
            R.drawable.love__980003
        )

        override fun getId() = 47

        override fun getTitle() = "Love"

        override fun getCategoryCode() = "USA-Love"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    LOVE_YOUR_LAWYER {
        override fun getImgs() = listOf(
            R.drawable.love_your_lawyer_day__680001,
            R.drawable.love_your_lawyer_day__680002
        )

        override fun getId() = 48

        override fun getTitle() = "Love your lawyer"

        override fun getCategoryCode() = "USA-Love-Your-Lawyer-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MARDI_GRAS {
        override fun getImgs() = listOf(
            R.drawable.mardi_gras__170001,
            R.drawable.mardi_gras__170002,
            R.drawable.mardi_gras__170003,
        )

        override fun getId() = 49

        override fun getTitle() = "Mardi gras"

        override fun getCategoryCode() = "USA-Mardi-Gras"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MAY_DAY {
        override fun getImgs() = listOf(
            R.drawable.may_day__240001,
            R.drawable.may_day__240002
        )

        override fun getId() = 49

        override fun getTitle() = "May day"

        override fun getCategoryCode() = "USA-May-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MEDICAL_ASSISTANTS_DAY {
        override fun getImgs() = listOf(
            R.drawable.medical_assistants_day__630001,
            R.drawable.medical_assistants_day__630002,
            R.drawable.medical_assistants_day__630003
        )

        override fun getId() = 50

        override fun getTitle() = "Medical assistants day"

        override fun getCategoryCode() = "USA-Medical-Assistants-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MEMORIAL_DAY {
        override fun getImgs() = listOf(
            R.drawable.memorial_day__130001,
            R.drawable.memorial_day__130002,
            R.drawable.memorial_day__130003
        )

        override fun getId() = 51

        override fun getTitle() = "Memorial day"

        override fun getCategoryCode() = "USA-Memorial-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MILITARY_FAMILY_MONTH {
        override fun getImgs() = listOf(
            R.drawable.military_family_month__670001,
            R.drawable.military_family_month__670002,
            R.drawable.military_family_month__670003,
            R.drawable.military_family_month__670004,
            R.drawable.military_family_month__670005
        )

        override fun getId() = 52

        override fun getTitle() = "Military family month"

        override fun getCategoryCode() = "USA-Military-Family-Month"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MINERS_DAY {
        override fun getImgs() = listOf(
            R.drawable.miners_day__730001,
            R.drawable.miners_day__730002
        )

        override fun getId() = 53

        override fun getTitle() = "Miners day"

        override fun getCategoryCode() = "USA-Miners-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MISS_YOU {
        override fun getImgs() = listOf(
            R.drawable.miss_you__990001,
            R.drawable.miss_you__990002,
            R.drawable.miss_you__990003
        )

        override fun getId() = 54

        override fun getTitle() = "Miss you"

        override fun getCategoryCode() = "USA-Miss-You"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MLK {
        override fun getImgs() = listOf(
            R.drawable.mlk__110001,
            R.drawable.mlk__110002,
            R.drawable.mlk__110003
        )

        override fun getId() = 55

        override fun getTitle() = "MLK"

        override fun getCategoryCode() = "USA-MLK"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    MOTHERS_DAY {
        override fun getImgs() = listOf(
            R.drawable.mothers_day__30001,
            R.drawable.mothers_day__30002,
            R.drawable.mothers_day__30003,
            R.drawable.mothers_day__30004,
            R.drawable.mothers_day__30005,
            R.drawable.mothers_day__30006
        )

        override fun getId() = 56

        override fun getTitle() = "Mothers day"

        override fun getCategoryCode() = "USA-Mothers-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    NELSON_MANDELA_INTER_DAY {
        override fun getImgs() = listOf(
            R.drawable.nelson_mandela_interday__530001,
            R.drawable.nelson_mandela_interday__530002,
            R.drawable.nelson_mandela_interday__530003
        )

        override fun getId() = 57

        override fun getTitle() = "Nelson Mandela InterDay"

        override fun getCategoryCode() = "USA-Nelson-Mandela-InterDay"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    NEW_YEAR {
        override fun getImgs() = listOf(
            R.drawable.new_year__100001,
            R.drawable.new_year__100002,
            R.drawable.new_year__100003,
            R.drawable.new_year__100004,
            R.drawable.new_year__100005
        )

        override fun getId() = 58

        override fun getTitle() = "New Year"

        override fun getCategoryCode() = "USA-New-Year"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    NURSES_DAY {
        override fun getImgs() = listOf(
            R.drawable.nurses_day__460001,
            R.drawable.nurses_day__460002
        )

        override fun getId() = 59

        override fun getTitle() = "Nurses day"

        override fun getCategoryCode() = "USA-Nurses-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    OKTOBERFEST {
        override fun getImgs() = listOf(
            R.drawable.oktoberfest__320001,
            R.drawable.oktoberfest__320002,
            R.drawable.oktoberfest__320003
        )

        override fun getId() = 60

        override fun getTitle() = "Oktoberfest"

        override fun getCategoryCode() = "USA-Oktoberfest"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PALM_SUNDAY {
        override fun getImgs() = listOf(
            R.drawable.palm_sunday__180001,
            R.drawable.palm_sunday__180002,
            R.drawable.palm_sunday__180003
        )

        override fun getId() = 61

        override fun getTitle() = "Palm sunday"

        override fun getCategoryCode() = "USA-Palm-Sunday"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PARALEGAL_DAY {
        override fun getImgs() = listOf(
            R.drawable.paralegal_day__640001,
            R.drawable.paralegal_day__640002,
            R.drawable.paralegal_day__640003
        )

        override fun getId() = 62

        override fun getTitle() = "Paralegal day"

        override fun getCategoryCode() = "USA-Paralegal-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PATRIKS_DAY {
        override fun getImgs() = listOf(
            R.drawable.patricks_day__90001,
            R.drawable.patricks_day__90002,
            R.drawable.patricks_day__90003,
            R.drawable.patricks_day__90004,
            R.drawable.patricks_day__90005,
            R.drawable.patricks_day__90006
        )

        override fun getId() = 63

        override fun getTitle() = "Patriks day"

        override fun getCategoryCode() = "USA-Patricks-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PATRIOT_DAY {
        override fun getImgs() = listOf(
            R.drawable.patriot_day__300001,
            R.drawable.patriot_day__300002
        )

        override fun getId() = 64

        override fun getTitle() = "Patriot day"

        override fun getCategoryCode() = "USA-Patriot-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PEAR_HARBOR_DAY {
        override fun getImgs() = listOf(
            R.drawable.pearl_harbor_remembrance_day__330001,
            R.drawable.pearl_harbor_remembrance_day__330002
        )

        override fun getId() = 65

        override fun getTitle() = "Pearl Harbor remembrance day"

        override fun getCategoryCode() = "USA-Pearl-Harbor-Remembrance-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PHARMACIST_DAY {
        override fun getImgs() = listOf(
            R.drawable.pharmacist_day__340001
        )

        override fun getId() = 66

        override fun getTitle() = "Pharmacist day"

        override fun getCategoryCode() = "USA-Pharmacist-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PIONEER_DAY {
        override fun getImgs() = listOf(
            R.drawable.pioneer_day__280001,
            R.drawable.pioneer_day__280002,
            R.drawable.pioneer_day__280003
        )

        override fun getId() = 67

        override fun getTitle() = "Peoneer day"

        override fun getCategoryCode() = "USA-Pioneer-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    POLICE_WOMAN_DAY {
        override fun getImgs() = listOf(
            R.drawable.police_woman_day__550001,
            R.drawable.police_woman_day__550002,
            R.drawable.police_woman_day__550003
        )

        override fun getId() = 68

        override fun getTitle() = "Police woman day"

        override fun getCategoryCode() = "USA-Police-Woman-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PROGRAMMERS_DAY {
        override fun getImgs() = listOf(
            R.drawable.programmers_day__560001,
            R.drawable.programmers_day__560002,
            R.drawable.programmers_day__560003
        )

        override fun getId() = 69

        override fun getTitle() = "Programmers day"

        override fun getCategoryCode() = "USA-Programmers-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PROMOTION {
        override fun getImgs() = listOf(
            R.drawable.promotion__970001,
            R.drawable.promotion__970002,
            R.drawable.promotion__970003
        )

        override fun getId() = 70

        override fun getTitle() = "Promotion day"

        override fun getCategoryCode() = "USA-Promotion"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    },
    PUBLICIST_DAY {
        override fun getImgs() = listOf(
            R.drawable.publicist_day__660001,
            R.drawable.publicist_day__660002,
            R.drawable.publicist_day__660003
        )

        override fun getId() = 71

        override fun getTitle() = "Publicist day"

        override fun getCategoryCode() = "USA-Publicist-Day"

        override fun getSubCategory() = emptyList<SubcategoryModel>()
    };


    abstract fun getImgs(): List<Int>
    abstract fun getId(): Int
    abstract fun getTitle(): String
    abstract fun getSubCategory(): List<SubcategoryModel>
    abstract fun getCategoryCode(): String
}
