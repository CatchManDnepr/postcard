package com.example.mailingpostcards.ui.catalog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseAdapter
import com.example.mailingpostcards.base.BaseKotlinFragment
import com.example.mailingpostcards.data.CategoryModel
import com.example.mailingpostcards.databinding.FragmentCatalogBinding
import com.example.mailingpostcards.ui.catalog.adapters.PostCardHolder
import com.example.mailingpostcards.utils.stub.StubViewModel
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * create an instance of this fragment.
 */
class CatalogFragment : BaseKotlinFragment() {


    private lateinit var binding: FragmentCatalogBinding
    override val layoutRes = R.layout.fragment_catalog
    override val viewModel: StubViewModel by viewModel()
    override val navigator: CatalogNavigator = get()

    private var category: CategoryModel? = null

    val adapter = BaseAdapter()
        .map(R.layout.item_postcard, PostCardHolder {
            navigator.goToSend(navController, bundleOf("category" to category?.title, "image" to getIntArray(it)))
        })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCatalogBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()


        binding.toolbar.ivBack.visibility = View.VISIBLE
        binding.toolbar.tvTitle.visibility = View.VISIBLE
        binding.toolbar.ivBack.setOnClickListener {
            navController?.popBackStack()
        }
        binding.toolbar.ivMenu.setOnClickListener {
            showPopupMenu(it)
        }


        category = arguments?.getParcelable("category") as? CategoryModel

        binding.toolbar.tvTitle.text = category?.title
        binding.rvContent.layoutManager = GridLayoutManager(context, 2)
        binding.rvContent.adapter = adapter

        Log.e("!!!", category.toString())
        adapter.itemsLoaded(category?.imgs ?: emptyList<Int>())

    }

    private fun showPopupMenu(v: View) {
        val popupMenu = PopupMenu(context, v)
        popupMenu.inflate(R.menu.popup)
        popupMenu
            .setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.about -> {
                        navigator.goToAbout(navController)
                        true
                    }
                    else -> false
                }
            }
        popupMenu.setOnDismissListener {

        }
        popupMenu.show()
    }

    fun getIntArray(it: Int): IntArray {
        val arr = ArrayList<Int>()

        arr.add(it)
        for(img in category!!.imgs) {
            if(img != it) {
                arr.add(img)
            }
        }

        return arr.toIntArray()
    }

}