 package com.example.mailingpostcards.ui.home.adapters

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.Holder
import com.example.mailingpostcards.data.SubcategoryModel
import java.util.*


 class SubcategoryHolder(private val onItemClick: (SubcategoryModel) -> Unit) : Holder<SubcategoryModel>() {

     override fun bind(itemView: View, item: SubcategoryModel) {
         itemView.findViewById<TextView>(R.id.tvSubCategory).text = item.title
         val root = itemView.findViewById<LinearLayout>(R.id.root)
         itemView.setOnClickListener {
             onItemClick.invoke(item)
         }
     }
}
