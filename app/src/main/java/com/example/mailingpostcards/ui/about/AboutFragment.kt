package com.example.mailingpostcards.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseKotlinFragment
import com.example.mailingpostcards.databinding.FragmentAboutBinding
import com.example.mailingpostcards.databinding.FragmentCatalogBinding
import com.example.mailingpostcards.utils.stub.StubNavigator
import com.example.mailingpostcards.utils.stub.StubViewModel
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * create an instance of this fragment.
 */
class AboutFragment : BaseKotlinFragment() {

    override val layoutRes = R.layout.fragment_about
    override val viewModel: StubViewModel by viewModel()
    override val navigator: StubNavigator = get()
    private lateinit var binding: FragmentAboutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAboutBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()


        binding.toolbar.ivBack.visibility = View.VISIBLE
        binding.toolbar.tvTitle.visibility = View.VISIBLE
        binding.toolbar.tvTitle.text = "About us"
        binding.toolbar.ivBack.setOnClickListener {
            navController?.popBackStack()
        }

    }

}