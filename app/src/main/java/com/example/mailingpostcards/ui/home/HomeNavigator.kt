package com.example.mailingpostcards.ui.home

import android.os.Bundle
import androidx.navigation.NavController
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseNavigator

class HomeNavigator: BaseNavigator() {

    fun goToCatalog(navController: NavController?, bundle: Bundle) {
        navController?.navigate(R.id.action_homeFragment_to_catalogFragment, bundle)
    }

    fun goToAbout(navController: NavController?) {
        navController?.navigate(R.id.action_homeFragment_to_aboutFragment)
    }
}