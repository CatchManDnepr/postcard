 package com.example.mailingpostcards.ui.catalog.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.Holder


 class PostCardHolder(private val onItemClick: (Int) -> Unit) : Holder<Int>() {
     override fun bind(itemView: View, item: Int) {
         itemView.findViewById<ImageView>(R.id.ivImage).setImageResource(item)
         itemView.setOnClickListener {
             onItemClick.invoke(item)
         }
        // itemView.findViewById<TextView>(R.id.tvTitle).text = item
     }
}
