package com.example.mailingpostcards.ui.send

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseKotlinFragment
import com.example.mailingpostcards.databinding.FragmentCatalogBinding
import com.example.mailingpostcards.databinding.FragmentHomeBinding
import com.example.mailingpostcards.databinding.FragmentSendBinding
import com.example.mailingpostcards.utils.stub.StubNavigator
import com.example.mailingpostcards.utils.stub.StubViewModel
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * create an instance of this fragment.
 */
class SendFragment : BaseKotlinFragment() {

    override val layoutRes = R.layout.fragment_send
    override val viewModel: StubViewModel by viewModel()
    override val navigator: StubNavigator = get()
    private lateinit var binding: FragmentSendBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSendBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        binding.toolbar.ivBack.visibility = View.VISIBLE
        binding.toolbar.tvTitle.visibility = View.VISIBLE
        binding.toolbar.tvTitle.text = arguments?.getString("category")
        binding.toolbar.ivBack.setOnClickListener {
            navController?.popBackStack()
        }

        val image = arguments?.getIntArray("image")

        val viewPagerAdapter = ViewPagerAdapter(requireContext(), image ?: intArrayOf())

        binding.ivPostcard.adapter = viewPagerAdapter
        binding.tvSend.setOnClickListener {
//            val uri = Uri.parse("android.resource://${resources.getResourceName(image ?: 0).replace(':','/')}")
//            val shareIntent: Intent = Intent().apply {
//                action = Intent.ACTION_SEND
//                putExtra(Intent.EXTRA_TEXT, "Send from MailingPostcards");
//                putExtra(Intent.EXTRA_STREAM, uri)
//                type = "image/jpeg"
//            }
//            startActivity(Intent.createChooser(shareIntent, "Send to:"))
        }
    }


}