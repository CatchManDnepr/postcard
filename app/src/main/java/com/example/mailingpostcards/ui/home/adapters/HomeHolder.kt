 package com.example.mailingpostcards.ui.home.adapters

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseAdapter
import com.example.mailingpostcards.base.Holder
import com.example.mailingpostcards.data.CategoryModel
import com.example.mailingpostcards.data.SubcategoryModel
import java.util.*


 class CategoryHolder(private val onItemClick: (CategoryModel) -> Unit,
                      private val onSubitemClick: (SubcategoryModel) -> Unit) : Holder<CategoryModel>() {



     override fun bind(itemView: View, item: CategoryModel) {
         val adapter = BaseAdapter()
             .map(R.layout.item_subcategory, SubcategoryHolder(onSubitemClick))

         itemView.findViewById<TextView>(R.id.tvTitle).text = item.title
         itemView.findViewById<ImageView>(R.id.ivSelebration).visibility = if(item.isSelebration) View.VISIBLE else View.GONE
         val rvSub = itemView.findViewById<RecyclerView>(R.id.rvSubcategory)
         val ivDrop = itemView.findViewById<ImageView>(R.id.ivDropDown)
         if (item.listOfSub.isNotEmpty()) {
             adapter.itemsLoaded(item.listOfSub)
             ivDrop.visibility = View.VISIBLE
             ivDrop.setOnClickListener {
                 if (rvSub.isVisible) {
                     rvSub.visibility = View.GONE
                     it.rotation = 90f
                 } else {
                     it.rotation = 270f
                     rvSub.visibility = View.VISIBLE
                 }
             }
             itemView.findViewById<TextView>(R.id.tvAll).visibility = View.VISIBLE
             itemView.findViewById<TextView>(R.id.tvAll).setOnClickListener {
                 onItemClick.invoke(item)
             }
             rvSub.visibility = View.VISIBLE
             rvSub.adapter = adapter

         } else {
             ivDrop.visibility = View.GONE
             itemView.findViewById<TextView>(R.id.tvAll).visibility = View.INVISIBLE
             rvSub.visibility = View.GONE
             itemView.setOnClickListener {
                 onItemClick.invoke(item)
             }
         }
         rvSub.visibility = View.GONE
         if (rvSub.isVisible) {
             ivDrop.rotation = 270f
         } else {
             ivDrop.rotation = 90f
         }


     }
}
