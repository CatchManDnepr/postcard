package com.example.mailingpostcards.ui.catalog

import android.os.Bundle
import androidx.navigation.NavController
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseNavigator

class CatalogNavigator: BaseNavigator() {

    fun goToSend(navController: NavController?, bundle: Bundle?) {
        navController?.navigate(R.id.action_catalogFragment_to_sendFragment, bundle)
    }

    fun goToAbout(navController: NavController?) {
        navController?.navigate(R.id.action_catalogFragment_to_aboutFragment)
    }
}