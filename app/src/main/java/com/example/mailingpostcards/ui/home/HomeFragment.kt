package com.example.mailingpostcards.ui.home

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.core.os.bundleOf
import com.applandeo.materialcalendarview.EventDay
import com.example.mailingpostcards.R
import com.example.mailingpostcards.base.BaseAdapter
import com.example.mailingpostcards.base.BaseKotlinFragment
import com.example.mailingpostcards.data.CategoryModel
import com.example.mailingpostcards.databinding.FragmentHomeBinding
import com.example.mailingpostcards.ui.MainActivity
import com.example.mailingpostcards.ui.home.adapters.CategoryHolder
import com.example.mailingpostcards.utils.Category
import com.example.mailingpostcards.utils.stub.StubViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DataSnapshot
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.DATE
import kotlin.collections.ArrayList


/**
 * create an instance of this fragment.
 */
class HomeFragment : BaseKotlinFragment() {

    private val dateFormat: SimpleDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

    val adapter = BaseAdapter()
        .map(R.layout.item_category, CategoryHolder ({
            FirebaseAnalytics.getInstance(requireContext()).logEvent("category_click", bundleOf("name" to it.title))
            navigator.goToCatalog(navController, bundleOf("category" to it))
        },{

            FirebaseAnalytics.getInstance(requireContext()).logEvent("subcategory_click", bundleOf("name" to it.title))
            navigator.goToCatalog(navController, bundleOf("category" to
                CategoryModel(title = it.title, imgs = it.imgs, id = -1, listOfSub = emptyList(), code = "", isSelebration = false)
            ))
        }))

    var firebaseDatas = ArrayList<DataSnapshot>()
    lateinit var categoryData: List<CategoryModel>

    private var events = mutableListOf<EventDay>()

    override val layoutRes = R.layout.fragment_home
    override val viewModel: StubViewModel by viewModel()
    override val navigator: HomeNavigator = get()
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()


        binding.rvContent.adapter = adapter
        binding.toolbar.ivMenu.setOnClickListener {
            showPopupMenu(it)
        }

        events = getFirebaseData()
        Handler().postDelayed({binding.cvCalendar.setEvents(events)}, 2000)


        binding.cvCalendar.setOnDayClickListener {
            adapter.itemsLoaded(getNewList(dateFormat.format(it.calendar.time)).toSet().toList())

            Log.e("!!!!!", events.toString())
            val newList = mutableListOf<EventDay>()

            newList.addAll(events)
            newList.add(EventDay(it.calendar, R.drawable.bacground_blue_rank))
            binding.cvCalendar.setEvents(newList)

            FirebaseAnalytics.getInstance(requireContext()).logEvent("calendar_click", bundleOf("date" to dateFormat.format(it.calendar.time)))
        }

        
    }

    private fun showPopupMenu(v: View) {
        val popupMenu = PopupMenu(context, v)
        popupMenu.inflate(R.menu.popup)
        popupMenu
            .setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.about -> {
                        navigator.goToAbout(navController)
                        true
                    }
                    else -> false
                }
            }
        popupMenu.setOnDismissListener {

        }
        popupMenu.show()
    }

    fun getFirebaseData(): MutableList<EventDay> {
        val list:MutableList<EventDay> = ArrayList()
        (activity as MainActivity).database.get().addOnSuccessListener {
            firebaseDatas?.addAll(it.children)

            for (dsp in firebaseDatas!!) {
                Log.e("!!!", "${dsp.key} - ${dsp.value}")
                try{
                    val calendar = Calendar.getInstance()
                    calendar.time = dateFormat.parse(dsp.value.toString())
                    list.add(EventDay(calendar, R.drawable.bacground_green_rank))
                } catch (e: Exception) {

                }


            }

            initAdapter()
            //val data = Gson().fromJson(it.value.toString(), FirebaseData::class.java)
            //Log.e("firebase", "Got value ${data}")
        }.addOnFailureListener{
            Log.e("firebase", "Error getting data", it)
        }

        events.clear()
        events.addAll(list)

        return list
    }

    fun getNewList(data: String): List<CategoryModel> {
        val set = ArrayList<CategoryModel>()
        Log.e("!!!>>", firebaseDatas.toString())
        val filterData = firebaseDatas?.filter {
            isDataConfirm(data, it.value.toString())
        }
        Log.e("!!!>>>>", filterData.toString())
        if(filterData == null || filterData.isEmpty()) {
            return categoryData
        }
        for (date in categoryData) {
            for (event in filterData) {
                if(event.key == date.code) {
                    set.add(date)
                }
            }

        }
        Log.e("!!!>>>>", set.toString())


        //set.addAll(categoryData.filter { set.filter { t -> it.code == t.code }.isEmpty() })
        return set
    }

    fun isDataConfirm(target: String, dataFirebase: String): Boolean {
        try {
            val dates = dataFirebase.split(",")
            dates.size == 1
            for (z in dates) {
                if(z.split(" ")[0] == "to") {
                    return true
                }
                Log.e("!!!>>", "$z==$target")
                if(target == z) {
                    return true
                }
            }
            //false
        } catch (e: Exception) {
            return true
        }
        return false
    }
    fun initAdapter() {
        val firebase = firebaseDatas.map { t -> t.key.toString() }
        categoryData = Category.values().map {


            val key = firebase.indexOf(it.getCategoryCode())

            var value = false
            for (dsp in firebaseDatas) {
                Log.e("@@@>>>", "${dsp.key} - ${dsp.value}")
                if (dsp.key.toString() == it.getCategoryCode()) {
                    value = (dsp.value.toString().split(",").size == 1
                            && dsp.value.toString().split(" ").size == 1)
                    Log.e("@@@", dsp.key.toString())
                }


            }

            CategoryModel(
                it.getId(),
                it.getTitle(),
                it.getImgs(),
                it.getCategoryCode(),
                it.getSubCategory(),
                value
                )
        }
        adapter.itemsLoaded(categoryData)
    }

}