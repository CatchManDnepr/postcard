package com.example.mailingpostcards

import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val tag = "FirebaseMessagingService"

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        println("$tag token --> $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        try {

            Log.e("!!!", remoteMessage.notification?.body.toString())
//            if (remoteMessage.notification != null) {
//                var testModel = Gson().fromJson(remoteMessage.notification?.body, Content::class.java)
//                showNotification(testModel.title, testModel.text, this,testModel.buttons ?: emptyList())
//            } else {
//                var testModel = Gson().fromJson(remoteMessage.data["message"], Content::class.java)
//                showNotification(testModel.title, testModel.text, this, testModel.buttons ?: emptyList())
//            }

        } catch (e: Exception) {
            println("$tag error -->${e.localizedMessage}")
        }
    }

//    private fun showNotification(
//        title: String?,
//        body: String?,
//        context: Context
//    ) {
//        val intent = Intent()
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        val pendingIntent = PendingIntent.getActivity(
//            context, 0, intent,
//            PendingIntent.FLAG_ONE_SHOT
//        )
//
//        val channelId = context.getString(R.string.channel_id)
//        val channelName = context.getString(R.string.channel_name)
//        val notificationManager =
//            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//
//        // Since android Oreo notification channel is needed.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            setupNotificationChannels(channelId, channelName, notificationManager)
//        }
//
//        val notificationIntent = Intent(context, MainActivity::class.java)
//
//        notificationIntent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TOP
//                or Intent.FLAG_ACTIVITY_SINGLE_TOP)
//
//        val newIntent = PendingIntent.getActivity(
//            context, 0,
//            notificationIntent, 0
//        )
//        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//
//        val notificationBuilder = NotificationCompat.Builder(context, channelId)
//            .setSmallIcon(R.mipmap.ic_launcher)
//            .setContentTitle(title)
//            .setContentText(body)
//            .setAutoCancel(true)
//            .setSound(soundUri)
//            .setContentIntent(newIntent)
//
//
//        for (i in buttons) {
//            val notifyIntent = Intent(context, MainActivity::class.java)
//
//            Log.e("!!!", i.toString())
//            val data = if(!i.handler.isNullOrEmpty()) {
//                Log.e("!!!", i.text.toString())
//                "javascript:"+ i.handler + "('" + i.param + "')"
//            } else {
//                ""
//            }
//
//            notifyIntent.putExtra("test", data)
//            intent.action = i.text
//            notifyIntent.putExtra("button", i.text)
//            notificationIntent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    or Intent.FLAG_ACTIVITY_SINGLE_TOP)
//
//            val newButtonIntent = PendingIntent.getActivity(
//                context, 0,
//                notifyIntent, PendingIntent.FLAG_IMMUTABLE
//
//            )
//
//            notificationBuilder.addAction(R.drawable.ic_launcher_foreground, i.text,
//                newButtonIntent)
//
//        }
//
//        notificationBuilder.setAutoCancel(true)
//        notificationManager.notify(0, notificationBuilder.build())
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupNotificationChannels(
        channelId: String,
        channelName: String,
        notificationManager: NotificationManager
    ) {

        val channel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW)
        channel.enableLights(true)
        channel.lightColor = Color.GREEN
        channel.enableVibration(true)
        notificationManager.createNotificationChannel(channel)

    }
}