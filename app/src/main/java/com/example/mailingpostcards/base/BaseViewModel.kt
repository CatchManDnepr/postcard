package com.example.mailingpostcards.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mailingpostcards.utils.coroutines.CoroutineHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel(
    private val app: Application
    ) : AndroidViewModel(app) {

    val onStartProgress: MutableLiveData<Unit> = MutableLiveData()
    val onEndProgress: MutableLiveData<Unit> = MutableLiveData()

    var showError: MutableLiveData<String> = MutableLiveData()

    val showDialog: MutableLiveData<Unit> = MutableLiveData()

    val restart: MutableLiveData<Unit> = MutableLiveData()

    val baseLogout: MutableLiveData<Boolean> = MutableLiveData()


    private var coroutineHelper: CoroutineHelper

    init {
        coroutineHelper = CoroutineHelper(viewModelScope)
    }

    protected open fun launch(
        onError: (e: Throwable) -> Unit,
        checkInternetConnection: Boolean = true,
        coroutineContext: CoroutineContext = Dispatchers.IO,
        block: suspend CoroutineScope.() -> Unit): Job {
        if(coroutineHelper == null) coroutineHelper = CoroutineHelper(viewModelScope)
        return coroutineHelper.launch(checkInternetConnection, coroutineContext, app, block, onError)
    }



    open fun onErrorHandler(throwable: Throwable) {

    }


}

