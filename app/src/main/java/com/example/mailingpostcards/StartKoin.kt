package com.example.mailingpostcards

import com.example.mailingpostcards.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


object KoinProvider {

    @JvmStatic
    fun startKoin(app: App) {
        startKoin {
            androidContext(app)
            modules(listOf(appModule, viewModelModule, navigatorsModule, sharedModule, repositoryModule))
        }
    }
}
