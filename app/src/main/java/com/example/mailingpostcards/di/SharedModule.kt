package com.example.mailingpostcards.di

import com.example.mailingpostcards.SharedPreferencesProvider
import org.koin.dsl.module

val sharedModule = module {
    single { SharedPreferencesProvider(get()) }
}