package com.example.mailingpostcards.di

import com.example.mailingpostcards.utils.stub.StubViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { StubViewModel(get()) }

}