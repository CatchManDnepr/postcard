package com.example.mailingpostcards.di

import com.example.mailingpostcards.ui.catalog.CatalogNavigator
import com.example.mailingpostcards.ui.home.HomeNavigator
import com.example.mailingpostcards.utils.stub.StubNavigator
import org.koin.dsl.module


val navigatorsModule = module {
    factory { StubNavigator() }
    factory { HomeNavigator() }
    factory { CatalogNavigator() }
}